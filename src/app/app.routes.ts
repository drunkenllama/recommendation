import { Routes } from '@angular/router';
import { RecommendationComponent } from './api/recommendation.component';
import { NoContentComponent } from './no-content';
import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '',      redirectTo: '/recommendations', pathMatch: 'full' },
  { path: 'recommendations',  component: RecommendationComponent },
  { path: 'recommendations/:subtopic',  component: RecommendationComponent },
  { path: '**',    component: NoContentComponent },
];
