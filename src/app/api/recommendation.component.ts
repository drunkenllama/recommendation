/* tslint:disable */
import {
  Component,
  OnInit
} from '@angular/core';
import { Http} from '@angular/http';
import { XLargeDirective } from './x-large';
import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';
import { Node } from './objects/node';
@Component({
  selector: 'recommendations',
  styleUrls: [ '../home/home.component.css' ],
  templateUrl: './recommendations.html'
})
export class RecommendationComponent implements OnInit {
  data;
  listens;
  sub;
  maxNum = 4;
  subtopics;
  nodeDict = {};
  recommendationDict = {};
  idToSubTopicsDict = {};
  recommendationList = [];
  subtopic:string;
  constructor(
    private http:Http,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  public ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
        if (params['subtopic']) {
          this.subtopic = String(params['subtopic']);
          console.log('X:', params['subtopic']);
        }
    });

    // given paramets of id of subtopics
    if (this.subtopic) {
      this.getRecommendedSubTopics(this.subtopic);
    }
  }

  getRecommendedSubTopics(UUID:string) {
    // create a graph of the listens history
    this.createGraph();
  } 

  createGraph() {
    this.nodeDict = {};
    this.http.get('assets/data/listens.json')
        .subscribe(res => {
          this.listens = res.json().data;

          for (let i = 1; i < this.listens.length; i++) {
            let now = this.listens[i].subtopic;
            let prev = this.listens[i-1].subtopic;

            if ( !this.nodeDict[prev] ) {
              this.nodeDict[prev] = {'subtopic': prev, 'next': {now: 1} };
            }

            if ( !this.nodeDict[now] ) {
              this.nodeDict[now] = {'subtopic': now, 'next': {} };
            }

            if (now != prev) {
              if (this.nodeDict[prev]['next'][now]) {
                this.nodeDict[prev]['next'][now] += 1;
              } else {
                this.nodeDict[prev]['next'][now] = 1;
              }
            }
          }
          // get sum of edges
          this.getRecommendations(this.subtopic, 4);

          this.recommendationList = [];

          // get hashmap of subtopics for lookup
          this.getSubtopicDict();

          //render
          this.data = {"data": this.recommendationList};
        });
  }

  getSubtopicDict() {
    this.http.get('assets/data/subtopics.json')
        .subscribe(res => {
          this.subtopics = res.json().data;
            if (this.subtopics && this.subtopics.length) {
              for (let i = 0; i < this.subtopics.length; i++) {
                this.idToSubTopicsDict[this.subtopics[i].id] = this.subtopics[i];
              }

            }
          let unsortedKeys = Object.keys(this.recommendationDict).map(function(key) {
            return { key: key, value: this[key] };
          }, this.recommendationDict);
          unsortedKeys.sort(function(p1, p2) { return p2.value - p1.value; });
          let topKeys = unsortedKeys.slice(0, this.maxNum);
          for (let i = 0; i < topKeys.length; i++) {
            this.recommendationList.push(this.idToSubTopicsDict[ String(topKeys[i].key) ]);
          }
        });
  }

  getRecommendations(UUID:string, max:number) {
    if (max == 0 || !this.nodeDict[UUID]) {
      return 0;
    }
    if (this.nodeDict[UUID]) {
        for (let key in this.nodeDict[UUID].next) {
          let sumEdges = this.nodeDict[UUID].next[key] + this.getRecommendations(key, max-1);
          this.recommendationDict[key] = sumEdges;
        }
    }
  }

}
